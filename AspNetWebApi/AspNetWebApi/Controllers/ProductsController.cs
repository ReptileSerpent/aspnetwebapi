﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AspNetWebApi.Models;
using System.Diagnostics;
using NuGet.Versioning;

namespace AspNetWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProjectDbContext _context;

        public ProductsController(ProjectDbContext context)
        {
            _context = context;
        }

        private static ProductDTO ProductToDTO(Product product)
        {
            var productDTO = new ProductDTO
            {
                Id = product.Id,
                Name = product.Name,
                Information = product.Information,
                Price = product.Price,
                Image = product.Image?.Base64Image,
                Category = "",
                ExtraAttributeValues = new List<ExtraAttributeValueDTO>()
            };
            if (product.Category != null)
                productDTO.Category = product.Category.Name;

            if (product.ExtraAttributeValues != null)
            {
                foreach (var extraAttributeValue in product.ExtraAttributeValues)
                    productDTO.ExtraAttributeValues.Add(new ExtraAttributeValueDTO()
                    {
                        Id = extraAttributeValue.Id,
                        ExtraAttributeDescription = new ExtraAttributeDescriptionDTO()
                        {
                            Id = extraAttributeValue.ExtraAttributeDescription.Id,
                            Name = extraAttributeValue.ExtraAttributeDescription.Name
                        },
                        Value = extraAttributeValue.Value
                    });
            }

            return productDTO;
        }

        /// <summary>
        /// GET all products ("api/Products")
        /// </summary>
        /// <param name="category"></param>
        /// <param name="extraAttributeDescription"></param>
        /// <param name="extraAttributeValue"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProducts([FromQuery] string? category, [FromQuery] string? extraAttributeDescription, [FromQuery] string? extraAttributeValue)
        {
            var filteredProducts = await _context.Products
                .Include(x => x.Category)
                .Include(x => x.Image)
                .Include(x => x.ExtraAttributeValues)!
                .ThenInclude(y => y.ExtraAttributeDescription).ToListAsync();

            if (category != null)
<<<<<<< 48e3a45dc2c8bd0bfb255b2f82fd0be72e1448f6
                // Category filter
=======
>>>>>>> a5cb397673c4025d8e367c3689613e689c8ffe64
                filteredProducts = filteredProducts.Where(x => x.Category?.Name == category).ToList();
            if (extraAttributeDescription != null)
            {
                // ExtraAttributeDescription and ExtraAttributeValue filter
                var descriptions = _context.ExtraAttributeDescriptions.Where(x => x.Name == extraAttributeDescription);
                if (descriptions.Count() == 0)
                    return BadRequest("Unknown extra attribute description: " + extraAttributeDescription);

                var value = _context.ExtraAttributeValues.Where(x => x.ExtraAttributeDescription.Name == extraAttributeDescription).ToList();
                if (extraAttributeValue != null)
                {
                    filteredProducts = filteredProducts.Where(product => product.ExtraAttributeValues.Any(
                        eav => eav.Value == extraAttributeValue &&
                        eav.ExtraAttributeDescription?.Name == extraAttributeDescription)).ToList();
<<<<<<< 48e3a45dc2c8bd0bfb255b2f82fd0be72e1448f6
                }
                else
                {
                    filteredProducts = filteredProducts.Where(product => product.ExtraAttributeValues.Any(
                        eav => eav.ExtraAttributeDescription?.Name == extraAttributeDescription)).ToList();
=======
>>>>>>> a5cb397673c4025d8e367c3689613e689c8ffe64
                }
            }

            return filteredProducts.Select(x => ProductToDTO(x)).ToList();
        }

        /// <summary>
        /// GET product ("api/Products/5")
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(int id)
        {
            var product = await _context.Products
                .Include(x => x.Category)
                .Include(x => x.Image)
                .Include(x => x.ExtraAttributeValues)!
                .ThenInclude(y => y.ExtraAttributeDescription)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (product == null)
                return NotFound();

            return ProductToDTO(product);
        }

        /// <summary>
        /// POST product ("api/Products")
        /// </summary>
        /// <param name="productDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ProductDTO>> PostProduct(ProductDTO productDTO)
        {
            var product = new Product
            {
                Name = productDTO.Name,
                Information = productDTO.Information,
                Price = productDTO.Price,
                Image = new ProductImage() { Base64Image = productDTO.Image },
                Category = await _context.Categories.FirstOrDefaultAsync(x => x.Name == productDTO.Category)
            };
            if (product.Category == null)
                return BadRequest("Category " + productDTO.Category + " does not exist in the database");

<<<<<<< 48e3a45dc2c8bd0bfb255b2f82fd0be72e1448f6
            product.ExtraAttributeValues = await ExtractExtraAttributeValuesFromDtoToList(productDTO);
=======
            var list = await ExtractExtraAttributeValuesFromDtoToList(productDTO);
                product.ExtraAttributeValues = list;
>>>>>>> a5cb397673c4025d8e367c3689613e689c8ffe64

            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetProduct),
                new { id = product.Id },
                ProductToDTO(product));
        }

        /// <summary>
        /// DELETE product ("api/Products/5")
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
                return NotFound();

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductExists(int id)
        {
            return (_context.Products?.Any(e => e.Id == id)).GetValueOrDefault();
        }

<<<<<<< 48e3a45dc2c8bd0bfb255b2f82fd0be72e1448f6
        /// <summary>
        /// Converts ExtraAttributeValuesDTO to ExtraAttributeValues
        /// </summary>
        /// <param name="productDTO"></param>
        /// <returns></returns>
=======
>>>>>>> a5cb397673c4025d8e367c3689613e689c8ffe64
        private async Task<List<ExtraAttributeValue>> ExtractExtraAttributeValuesFromDtoToList(ProductDTO productDTO)
        {
            var list = new List<ExtraAttributeValue>();

            if (productDTO.ExtraAttributeValues == null)
                return list;

<<<<<<< 48e3a45dc2c8bd0bfb255b2f82fd0be72e1448f6
=======
            var delimiterChars = new string[] { ": ", ", " };
            var extraAttributeValues = productDTO.ExtraAttributeValues;

>>>>>>> a5cb397673c4025d8e367c3689613e689c8ffe64
            foreach (var extraAttributeValueDTO in productDTO.ExtraAttributeValues)
            {
                var extraAttributeValue = new ExtraAttributeValue();
                extraAttributeValue.Id = extraAttributeValueDTO.Id;
                extraAttributeValue.ExtraAttributeDescription = await _context.ExtraAttributeDescriptions.FirstOrDefaultAsync(x =>
                    x.Id == extraAttributeValueDTO.ExtraAttributeDescription.Id &&
                    x.Name == extraAttributeValueDTO.ExtraAttributeDescription.Name);
                extraAttributeValue.Value = extraAttributeValueDTO.Value;
                list.Add(extraAttributeValue);
            }
            return list;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("/error")]
        public IActionResult HandleError() => Problem();
    }
}
