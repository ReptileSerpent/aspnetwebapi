﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApi.Models
{
    public class ProductImage
    {
        public int Id { get; set; }
        [Required]
        public string Base64Image { get; set; } = string.Empty;
    }
}
