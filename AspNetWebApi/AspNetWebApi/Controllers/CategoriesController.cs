﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AspNetWebApi.Models;
using System.Text.Json;

namespace AspNetWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ProjectDbContext _context;

        public CategoriesController(ProjectDbContext context)
        {
            _context = context;
        }

        private static CategoryDTO CategoryToDTO(Category category)
        {
            var categoryDTO = new CategoryDTO();
            categoryDTO.Id = category.Id;
            categoryDTO.Name = category.Name;
            categoryDTO.ExtraAttributeDescriptions = new List<ExtraAttributeDescriptionDTO>();

            if (category.ExtraAttributeDescriptions == null)
                categoryDTO.ExtraAttributeDescriptions = new List<ExtraAttributeDescriptionDTO>();
            else
            {
<<<<<<< 48e3a45dc2c8bd0bfb255b2f82fd0be72e1448f6
=======
                var descriptionCount = category.ExtraAttributeDescriptions.Count;
>>>>>>> a5cb397673c4025d8e367c3689613e689c8ffe64
                foreach (var extraAttributeDescription in category.ExtraAttributeDescriptions)
                {
                    categoryDTO.ExtraAttributeDescriptions.Add(new ExtraAttributeDescriptionDTO
                    {
                        Id = extraAttributeDescription.Id,
                        Name = extraAttributeDescription.Name
                    });
                }
            }

            return categoryDTO;
        }

        /// <summary>
        /// GET all categories ("api/Categories")
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetCategoryItems()
        {
            return await _context.Categories
                .Include(x => x.ExtraAttributeDescriptions)
                .Select(x => CategoryToDTO(x))
                .ToListAsync();
        }

        /// <summary>
        /// GET category ("api/Categories/5")
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetCategory(int id)
        {
            var category = await _context.Categories
                .Include(x => x.ExtraAttributeDescriptions)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (category == null)
            {
                return NotFound();
            }

            return CategoryToDTO(category);
        }

        /// <summary>
        /// POST category ("api/Categories")
        /// </summary>
        /// <param name="categoryDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CategoryDTO>> PostCategory(CategoryDTO categoryDTO)
        {
            var category = new Category
            {
                Name = categoryDTO.Name,
                ExtraAttributeDescriptions = ExtraAttributeDescriptionsFromDtoToList(categoryDTO.ExtraAttributeDescriptions)
            };

            _context.Categories.Add(category);
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetCategory),
                new { id = category.Id },
                CategoryToDTO(category));
        }

        /// <summary>
        /// PUT category ("api/Categories")
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(int id, CategoryDTO categoryDTO)
        {
            if (id != categoryDTO.Id)
            {
                return BadRequest("Id mismatch");
            }

            var category = await _context.Categories.Include(x => x.ExtraAttributeDescriptions).FirstOrDefaultAsync(i => i.Id == id);
            if (category == null)
            {
                return NotFound("No category with specified id");
            }

            category.Name = categoryDTO.Name;
            category.ExtraAttributeDescriptions = ExtraAttributeDescriptionsFromDtoToList(categoryDTO.ExtraAttributeDescriptions);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!CategoryExists(id))
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// DELETE category ("api/Categories/5")
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var category = await _context.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CategoryExists(int id)
        {
            return (_context.Categories?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private List<ExtraAttributeDescription> ExtraAttributeDescriptionsFromDtoToList(List<ExtraAttributeDescriptionDTO> extraAttributeDescriptionsDTO)
        {
            var list = new List<ExtraAttributeDescription>();

            if (extraAttributeDescriptionsDTO == null)
<<<<<<< 48e3a45dc2c8bd0bfb255b2f82fd0be72e1448f6
                return list;

            var dtoExtraAttributeDescriptions = extraAttributeDescriptionsDTO;
=======
                return list;

            var dtoExtraAttributeDescriptions = extraAttributeDescriptionsDTO;
            if (dtoExtraAttributeDescriptions == null)
                return list;

>>>>>>> a5cb397673c4025d8e367c3689613e689c8ffe64
            foreach (var description in dtoExtraAttributeDescriptions)
            {
                list.Add(new ExtraAttributeDescription()
                {
                    Id = description.Id,
                    Name = description.Name
                });
            }
            return list;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("/error")]
        public IActionResult HandleError() => Problem();
    }
}
