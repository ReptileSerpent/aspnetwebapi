﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApi.Models
{
    public class ProductDTO
    {
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        public string Name { get; set; } = string.Empty;
        public string? Information { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        public string? Image { get; set; }
        [Required]
        [MinLength(1)]
        public string Category { get; set; } = string.Empty;
        public List<ExtraAttributeValueDTO>? ExtraAttributeValues { get; set; }
    }
}
