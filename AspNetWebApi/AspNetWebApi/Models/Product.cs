﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApi.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        public string Name { get; set; } = string.Empty;
        public string? Information { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        public ProductImage? Image { get; set; }
        [Required]
        public Category? Category { get; set; }
        public ICollection<ExtraAttributeValue>? ExtraAttributeValues { get; set; }
    }
}
