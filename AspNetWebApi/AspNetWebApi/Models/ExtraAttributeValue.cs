﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApi.Models
{
    public class ExtraAttributeValue
    {
        public int Id { get; set; }
        [Required]
        public ExtraAttributeDescription? ExtraAttributeDescription { get; set; }
        [Required]
        [MinLength(1)]
        public string Value { get; set; } = string.Empty;
    }
}
