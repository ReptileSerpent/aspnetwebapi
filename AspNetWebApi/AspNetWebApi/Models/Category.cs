﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApi.Models
{
    public class Category
    {
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        public string Name { get; set; } = string.Empty;
        public ICollection<ExtraAttributeDescription>? ExtraAttributeDescriptions { get; set; }
    }
}
