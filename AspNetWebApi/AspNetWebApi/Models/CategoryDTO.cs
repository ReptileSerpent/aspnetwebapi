﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApi.Models
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        public string Name { get; set; } = string.Empty;
        public List<ExtraAttributeDescriptionDTO>? ExtraAttributeDescriptions { get; set; }
    }
}
