﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApi.Models
{
    public class ExtraAttributeValueDTO
    {
        public int Id { get; set; }
        [Required]
        public ExtraAttributeDescriptionDTO? ExtraAttributeDescription { get; set; }
        [Required]
        [MinLength(1)]
        public string Value { get; set; } = string.Empty;
    }
}
