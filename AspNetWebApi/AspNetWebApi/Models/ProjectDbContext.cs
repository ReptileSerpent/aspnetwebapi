﻿using Microsoft.EntityFrameworkCore;

namespace AspNetWebApi.Models
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; } = null!;
        public DbSet<Product> Products { get; set; } = null!;
        public DbSet<ExtraAttributeDescription> ExtraAttributeDescriptions { get; set; } = null!;
        public DbSet<ExtraAttributeValue> ExtraAttributeValues { get; set; } = null!;
    }
}